# Robo Scanner

Scan an IP Address or range looking for webservers with a robots.txt file.

Usage: python3 robo_scan.py 192.168.1.1-254
