#!/usr/bin/env python3
import requests
from sys import argv, stdout


class RoboScan:
	def set_range(self, raw_range):
		chunks = raw_range.split(".")
		subnet = "{}.{}.{}".format(chunks[0], chunks[1], chunks[2])
		get_range = chunks[3]
		if "-" not in get_range:
			start = int(chunks[3])
			end = int(chunks[3])
		else:
			start = int(get_range.split("-")[0])
			end = int(get_range.split("-")[1]) + 1
		return subnet, start, end

	def scan_robots(self, subnet, start, end):
		robot_files = []
		headers = {'User-Agent': 'Googlebot'}
		if start == end:
			try:
				robots = "http://{}.{}/robots.txt".format(subnet, start)
				stdout.write("Testing {}\r".format(robots))
				response = requests.get(robots, timeout=.5, headers=headers)
				if response.status_code == 200:
					print("[+] Found {}            ".format(robots))
					robot_files.append([robots, response.text])
			except:
				pass
		else:
			for i in range(start, end):
				robots = "http://{}.{}/robots.txt".format(subnet, i)
				try:
					stdout.write("Testing {}\r".format(robots))
					response = requests.get(robots, timeout=.5, headers=headers)
					if response.status_code == 200:
						print("[+] Found {}          ".format(robots))
						robot_files.append([robots, response.text])
				except:
					pass
		return robot_files


def main():
	robo = RoboScan()
	print("Scan an IP Address or range looking for robots.txt files.")
	try:
		subnet, start, end = robo.set_range(argv[1])
		results = robo.scan_robots(subnet, start, end)
		print("Scan completed.                          \n")
		if len(results) > 0:
			choice = input("Would you like to view the robots.txt files? ")
			if choice.lower() in ("y", "yes"):
				for line in results:
					print("\n{}:".format(line[0]))
					print("{}".format(line[1]))
					if line != results[-1]:
						input("Press enter to view the next file.")

	except IndexError:
		print("\n[!] Please supply an IP Address or range.\n")
		print("Example: python3 {} 192.168.1.1-254".format(argv[0]))

if __name__ == "__main__":
	main()
